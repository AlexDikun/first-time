import numpy as np
#import matplotlib.pyplot as plt


teta = np.radians([-90, 0, -90, 90, -180, 90, -180, 0])
print("teta0 = ", np.degrees(teta))
r = [435, 0, 0, 12, 12, 150, 0, 90]
print("r = ", r)
alpha = np.radians([-5, -90, 90, 90, 90, 0, 90, 0])
print("alpha = ", np.degrees(alpha))
d = [210, 0, 0, 290, 0, -34, 275, 0]
print("d = ", d)

def Ai(i):
	global r, alpha, d, teta
	ct= np.cos(teta[i])
	ca= np.cos(alpha[i])
	st= np.sin(teta[i])
	sa= np.sin(alpha[i])
	A=np.array([[ct, -ca*st,  sa*st, r[i]*ct],
        		[st,  ca*ct, -sa*ct, r[i]*st],
        		[0,   sa,     ca,    d[i]],
        		[0,   0,      0,     1]])
	return A

def dAi(i):
	global r, alpha, d, teta
	ct= np.cos(teta[i])
	ca= np.cos(alpha[i])
	st= np.sin(teta[i])
	sa= np.sin(alpha[i])
	dA=np.array([[-st, -ca*ct,  sa*ct, -r[i]*st],
         		 [ ct, -ca*st,  sa*st,  r[i]*ct],
         		 [ 0,   0,      0,      0],
         		 [ 0,   0,      0,      0]])
	return dA


def main():
	global r, alpha, d, teta

	N = 8
	tf = 50.
	h = 0.001
	t = 0.
	Tupr=0.01

	T=np.array([[0, 1, 0, -0.21], [1, 0, 0, 0.52], [0, 0, -1, 0.93], [0, 0, 0, 1]])# NLEQ

	ai=np.zeros((N,4,4))
	dai=np.zeros((N,4,4))

	while t < tf :
		for i in range(N):
			ai[i] = Ai(i)
			dai[i]= dAi(i)

		delta = np.eye(4)
		for i in range(N):
			delta = delta.dot(ai[i])
		delta = (delta[0:3]-T[0:3]).T.reshape(-1)
		#print("delta=",delta)

		ddelta=np.zeros((N, 12))
		for i in range(N):
			tempo = np.eye(4)
			for j in range(N):
				if i==j :
					tempo = tempo.dot(dai[j])
				else :
					tempo = tempo.dot(ai[j])
			ddelta[i] = tempo[0:3].T.reshape(-1)
		#print("ddelta=",ddelta)

		G = 2*np.dot(ddelta, delta)
		gPlus = G/(np.dot(G, G)+0.01)
		Crit = np.dot(delta, delta)
		u = gPlus*(-Crit)/Tupr
		#print("u=",u)

		teta += u*h # solution of a differential equation

		t += h

	print("Crit = ", Crit)
	print("tetaf = ", np.degrees(teta))#'''
	delta = np.eye(4)
	for i in range(N):
		delta = delta.dot(Ai(i))

	print ("T = \n", T)
	print ("P = \n", delta)
	print ("T - P = \n", T-delta)



if __name__ == '__main__':
	main()
